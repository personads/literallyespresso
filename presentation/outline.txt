presentation outline

Dauer: 10-15 Min


Titel

Espresso-Algorithmus
	Definition
	Phasen
		Patterninduktion
		Patterakquisition
		Instanzakquisition
		Pattern- und Instanzranking
			pmi
			rpi
			rt
		Patternfilterung
		Iterationsevaluierung

Daten und Tools
	Python 3.4
	TC Wikipedia Corpus NYU
	Relationen

Schwierigkeiten und Verbesserungen
	rpi, rt und pmi Berechnungen (vorweg nehmen?)
	Anapherresolution
	Passivkonstruktionen
	Das MEGA-OUTOFMEMORY-Korpus

Implementierung
	corpus_reader
	pattern
	instance
	pattern_collection
	espresso

Live Demo

Referenzen

Fragen

Vielen Dank