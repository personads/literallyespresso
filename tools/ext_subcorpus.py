'''
Subcorpus Extractor

divides a larger corpus into a smaller subcorpus for easier processing

ext_subcorpus.py file_in line_lower_bound line_upper_bound file_out
'''
from sys import argv, stdout

if len(argv) > 4 :
	print()
	line_lower_bound = int(argv[2])
	line_upper_bound = int(argv[3])
	
	stdout.write('\rextracting subcorpus (lines ' + str(line_lower_bound) + ' to ' + str(line_upper_bound) + ')...')
	stdout.flush()
	
	line_n = 0
	with open(argv[1], encoding='utf8') as fop_r, open(argv[4], 'w', encoding='utf8') as fop_w :
		for line in fop_r :
			if (line_n%10000) == 0:
				stdout.write('\rextracting subcorpus (lines ' + str(line_lower_bound) + ' to ' + str(line_upper_bound) + ')...line %d' % (line_n))
				stdout.flush()
			if (line_n >= line_lower_bound) and (line_n < line_upper_bound) :
				fop_w.write(line)
			elif line_n >= line_upper_bound :
				break
			line_n += 1
	
	stdout.write('\rextracting subcorpus (lines ' + str(line_lower_bound) + ' to ' + str(line_upper_bound) + ')...done (%d lines extracted)' % (line_upper_bound-line_lower_bound))
	stdout.flush()
	print()
else :
	print(__doc__)