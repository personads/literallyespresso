'''
Document Extractor

extract document titles and line-positions

ext_docs.py file_in file_out
'''
from sys import argv, stdout

if len(argv) > 2 :
	print()
	stdout.write('\rextracting document titles...')
	stdout.flush()
	
	line_n = 0
	with open(argv[1], encoding='utf8') as fop_r, open(argv[2], 'w', encoding='utf8') as fop_w :
		for line in fop_r :
			if (line_n%10000) == 0 :
				stdout.write('\rextracting document titles...line %d' % (line_n))
				stdout.flush()
			if line.startswith('#s-doc') :
				fop_w.write(line + '\t' + str(line_n))
			line_n += 1
	
	stdout.write('\rextracting document titles...done (%d lines processed)' % (line_n))
	stdout.flush()
	print()
else :
	print(__doc__)