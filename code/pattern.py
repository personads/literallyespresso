class Pattern :
	"""
	Representation of a pattern for a semantic relation
	
	A pattern consists of the regular expression (regex) it is generalized to (such as [term] likes [term]), 
	its rpi (reliabilty score, computed in the pattern_collection class)
	and a locked marker (True or False) to prevent duplicate instace association on further iteration
	Patterns are sortable by their rpi
	"""
	def __init__(self, regex, rpi=None, locked=False) :
		"""
		initializes a new pattern
		
		Standard values: 
		rpi = None (will be calculated later); seed patterns are initialized with rpi = 1
		locked = False
		"""
		self.regex = regex
		self.rpi = rpi
		self.next_rpi = None
		self.locked = locked
	
	def __str__(self) :
		return self.regex + ' (rpi:' + str(self.rpi) + ') ' + '(locked:' + str(self.locked) + ')'

	def __hash__(self) :
		return hash(self.regex)
		
	def __eq__(self, other) :
		res = False
		if type(other) is Pattern :
			res = (self.regex == other.regex)
		return res
