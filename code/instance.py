class Instance :
	"""
	Representation of an instance of a general pattern for a semantic relation
	
	self.data: a tuple of the entities in a given relation e.g (everybody, chocolate) for "everybody likes chocolate"
	self.pmi: the pointwise mutual information this instance has with the patterns it is extracted by
	self.rt: reliability score of this instance pair (computed in pattern_collection)
	"""
	def __init__(self, data, pmi=None, rt=None, seed=False) :
		"""
		Initializes the instance
		
		default value for pmi, rt = None; rt is manually set as 1 for seed instances
		"""
		self.data = data
		self.pmi = pmi
		self.rt = rt
		self.seed = seed
	
	def __str__(self) :
		return str(self.data) + ' (rt:' + str(self.rt) + ')' + ' (pmi:' + str(self.pmi) + ')'
	
	def __hash__(self) :
		return hash(self.data)
	
	def __eq__(self, other) :
		res = False
		if type(other) is Instance :
			res = (self.data == other.data)
		return res
