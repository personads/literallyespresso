from pattern import Pattern
from instance import Instance
from espresso import Espresso
from sommelier import Sommelier

seed = {Pattern(r'(((\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)+(\w+/IN\s)?(\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)*)?\w+/NN(S|PS?)?) ist/VBD ein/DT (((\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)+(\w+/IN\s)?(\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)*)?\w+/NN(S|PS?)?)', rpi=1.) : [Instance(('Kaesebrot/NN', 'gutes/JJ Brot/NN'), rt=1., seed=True)] }
#could this somehow be constructed from strings in the espresso class so pattern and instance don't have to be imported here?
testpresso = Espresso('../data/tst_sen_corpus.txt', seeds=seed)
testpresso.run(2)
testpresso.out('tst_espresso_out.txt')
som = Sommelier(testpresso) # or a text file containing espresso.serialize()
som.taste()