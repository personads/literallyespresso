from corpus_reader import CorpusReader
from instance import Instance
from pattern import Pattern
from pattern_collection import PatternCollection
from espresso import Espresso
import re

class Sommelier :
	def __init__(self, espresso) :
		self.precision = None
		self.patterns = None
		self.term_regex = None
		self.corpus = None
		if type(espresso) is not Espresso :
			espresso = Espresso.load(espresso)
		self.patterns = espresso.patterns
		self.term_regex = espresso.term_regex
		self.corpus = CorpusReader(espresso.corpus_file, mode=espresso.corpus_mode)
	
	def taste(self) :
		print('\nSommelier Evaluation Tool\n')
		print('You are currently tasting espresso from the fields of "' + self.corpus.file + '"\n')
		eval_results = []
		# make self-evaluation joke
		for line in self.corpus:
			instances = self.patterns.is_instance_match(line)
			if instances != []:
				for instance in instances:
					instancepatterns = self.patterns.get_patterns_by_instance(instance)
					for pattern in instancepatterns:
						if re.search(pattern.regex, line):
							evaluee = '\n' + pattern.regex.replace(self.term_regex, '[...]') + '\t' + str(instance.data)
							print(evaluee)
							print('"' + line.replace('\n', '\\n') + '"')
							evaluation = input('Does it taste True (t) or False (f)? ')
							while evaluation not in ['t', 'f'] :
								evaluation = input('Pardon (t/f)? ')
							eval_results.append(evaluation)
		if len(eval_results) > 0 :
			tp = eval_results.count('t')
			fp = eval_results.count('f')
			self.precision = tp/tp+fp