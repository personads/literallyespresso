from pattern import Pattern
from instance import Instance
import re
import math

class PatternCollection :
	"""
	A collection of generic relation patterns and the instances extracted by these patterns
	
	data: a dictionary with pattern as key and a list of the extracted instances as value
	term_regex: a generic regular expression that is substituted for entities in pattern creation
				default regex: ([a-zA-Z])
	avg_rpi: the average pattern reliability score of all the patterns saved in data
	"""
	def __init__(self, data={}, term_regex=r'([a-zA-Z])') :
		self.data = data
		self.term_regex = term_regex
		self.max_pmi = None # used to store the maximum pmi between all patterns and instances
	
	def __str__(self) :
		res = ''
		for pattern in self.data :
			res += str(pattern).replace(self.term_regex, '[...]') + '\n' # replace term_regex for readability
			for instance in self.data[pattern] :
				res += '\t' + str(instance) + '\n'
		return res
	
	def __len__(self) :
		return len(self.data)
	
	def serialize(self) :
		"""returns the pattern collection in XML-like format"""
		res = '<collection count_patterns="' + str(len(self)) + '" count_instances="' + str(len(self.get_instances())) + '" avg_rpi="' + str(self.avg_rpi()) + '" term_regex="' + self.term_regex.replace('"', '\'')  +'">\n'
		for pattern in self.data :
			res += '\t<pattern regex="' + pattern.regex.replace('"', '\'') + '" rpi="'+ str(pattern.rpi) + '" locked="' + str(pattern.locked) + '">\n'
			for instance in self.data[pattern] :
				res += '\t\t<instance rt="' + str(instance.rt) + '" seed="' + str(instance.seed) + '">\n'
				for term in instance.data :
					res += '\t\t\t<term data="' + term.replace('"', '\'') + '"></term>\n'
				res += '\t\t</instance>\n'
			res += '\t</pattern>\n'
		res += '</collection>'
		return res
	
	@staticmethod
	def load(file) :
		"""deserializes a PatternCollection class"""
		res = None
		with open(file, 'r') as fop :
			try :
				ext_attr = re.compile(r'\s+([^\s]+?)="(.+?)"')
				collection_data = {}
				lines = fop.readlines()
				line_n = 0
				while line_n < len(lines) :
					if re.match(r'\s*<collection\s.*', lines[line_n]) :
						term_regex = None
						for attr in ext_attr.findall(lines[line_n]) :
							if attr[0] == 'term_regex' :
								term_regex = attr[1].replace('\'', '"')
						res = PatternCollection(term_regex=term_regex)
						while (not re.match(r'\s*</collection>.*', lines[line_n])) and (line_n < len(lines)-1) :
							if re.match(r'\s*<pattern\s.*', lines[line_n]) :
								regex = None
								rpi = None
								locked = None
								for attr in ext_attr.findall(lines[line_n]) :
									if attr[0] == 'regex' :
										regex = attr[1]
									if attr[0] == 'rpi' :
										if attr[1] != 'None' :
											rpi = float(attr[1])
									if attr[0] == 'locked' :
										if attr[1] == 'True' :
											locked = True
										elif attr[1] == 'False' :
											locked = False
								cur_pattern = Pattern(regex, rpi, locked)
								res.add_pattern(cur_pattern)
								while (not re.match(r'\s*</pattern>.*', lines[line_n])) and (line_n < len(lines)-1) :
									if re.match(r'\s*<instance\s.*', lines[line_n]) :
										rt = None
										seed = None
										for attr in ext_attr.findall(lines[line_n]) :
											if attr[0] == 'rt' :
												if attr[1] != 'None' :
													rt = float(attr[1])
											if attr[0] == 'seed' :
												if attr[1] == 'True' :
													seed = True
												elif attr[1] == 'False' :
													seed = False
										instance_data = []
										while (not re.match(r'\s*</instance>.*', lines[line_n])) and (line_n < len(lines)-1) :
											for attr in ext_attr.findall(lines[line_n]) :
												if attr[0] == 'data' :
													instance_data.append(attr[1])
											line_n += 1
										cur_instance = Instance(tuple(instance_data), rt=rt, seed=seed)
										res.data[cur_pattern].append(cur_instance)
									line_n += 1
							line_n += 1
					line_n += 1
			except Exception as ex:
				print('Error : Could not load "' + file + '" as PatternCollection :/')
				print(ex)
		return res

	def get_patterns(self) :
		"""returns a list of the patterns in the pattern collection"""
		return list(self.data)
	
	def add_pattern(self, pattern) :
		"""
		adds a pattern to the data dictionary
		
		if the pattern isn't already in the dictionary, adds it as new key with empty list as value
		"""
		if type(pattern) is Pattern :
			self.data.setdefault(pattern, [])
	
	def has_pattern(self, pattern) :
		"""checks if a pattern is already in the collection"""
		return pattern in self.data
	
	def ext_patterns(self, line, instance) :
		"""
		extracts a generic pattern from a corpus line given an Instance that occurs with this pattern 
		
		selects the text in between both entities and replaces entities with the generic term_regex given at initalisation
		limited to active constructions only (X [pattern] Y), conjunctions may also produce unexpected results (X pat1 (Y and X) pat2 Y)
		"""
		res = []
		for match in re.findall(instance.data[0] + '((?:(?!(' + instance.data[0] + '|' + instance.data[1] + ')).)+)' + instance.data[1], line) : # finds closest matches of X and Y, e.g. (X bla X bla Y) -> X bla (X bla Y)
			regex = self.term_regex + '(' + re.escape(match[0]) + ')' + self.term_regex
			res.append(Pattern(regex))
		return res
		# does not match X pat1 (Y and X) pat2 Y or passive etc.
		# wer das liess?t, ist suess ^^
		# wer das liest, kann kein Deutsch :P :*
		# mimimi :*
	
	def lock_patterns(self) :
		"""locks all patterns currently in the collection to prevent duplicate instance association on further iteration"""
		for pattern in self.data.keys() :
			pattern.locked = True
	
	def get_patterns_by_instance(self, instance) :
		"""returns all patterns associated with an instance"""
		res = []
		for pattern, instances in self.data.items() :
			if instance in instances :
				res.append(pattern)
		return res
	
	def is_pattern_match(self, line) :
		"""returns all known patterns found in a given line"""
		res = []
		patterns = self.get_patterns()
		for pattern in patterns:
			if re.search(pattern.regex, line):
				res.append(pattern)
		return res
	
	def get_instances(self) :
		"""returns a list of all instances in collection, regardless of which pattern they are associated with"""
		return [val for sublist in self.data.values() for val in sublist]
	
	def add_instance(self, instance, pattern) :
		"""adds an instance occurence to a pattern's data and prints an error message if the pattern is not in the collection"""
		if (type(instance) is Instance) and (type(pattern) is Pattern) :
			try :
				if not pattern.locked :
					self.data[pattern].append(instance)
			except KeyError :
				print('Error : Could not add Instance. Pattern "' + str(pattern) + '" is not in PatternCollection')
	
	def ext_instances(self, line, pattern) :
		"""
		extracts the entites in an occurence of a generic pattern for a binary relation
		
		creates a tuple from the two groups matched by the pattern's regular expression
		"""
		res = []
		pattern_relation = pattern.regex.replace(self.term_regex, '')
		pattern_relation = pattern_relation[1:len(pattern_relation)-1]
		instance_data = []
		for match in re.findall(pattern.regex, line) :
			to_append = True
			for group in match :
				if len(instance_data) > 1 : # break after instance_data is filled with two terms
					break
				if to_append :
					instance_data.append(group)
					to_append = False
				if re.escape(group) == pattern_relation :
					to_append = True
			instance_data = tuple(instance_data)
			res.append(Instance(instance_data))
			instance_data = []
		return res
	
	def count_instance(self, instance) :
		"""counts how often an instance is found in the collection across all patterns"""
		res = 0
		for instances in self.data.values() :
			res += instances.count(instance)
		return res
	
	def is_instance_match(self, line) :
		"""checks a corpus line for any occurence of one of the instances already in the pattern collection"""
		res = []
		for instance in set(self.get_instances()) :
			if ( instance.data[0] in line ) and ( instance.data[1] in line ) :
				res.append(instance)
		return res
	
	def rank(self) :
		"""computes patterns' rpi and instances' rt"""
		# calculate max_pmi over all patterns and instances
		for pattern in self.data :
			for instance in self.data[pattern]:
				cur_pmi = self.pmi(instance, pattern)
				if ( self.max_pmi == None ) :
					self.max_pmi = cur_pmi
				if ( cur_pmi > self.max_pmi ):
					self.max_pmi = cur_pmi
		# calculate rpi for all patterns
		for pattern in self.data :
			pattern.next_rpi = self.rpi(pattern)
		# push new rpi values for all patterns
		for pattern in self.data :
			pattern.rpi = pattern.next_rpi
	
	def select(self, k) :
		"""shortens the pattern collection to k most-reliable entries"""
		if ( k < len(self.data) ) :		
			cur_patterns = self.get_patterns() # list of all patterns
			sort_patterns = sorted(cur_patterns, key = lambda pattern : pattern.rpi) # sort list by rpi
			sort_patterns = sort_patterns[k:] # truncate to top k
			for pattern in cur_patterns : # remove all patterns not in the top k
				if pattern not in sort_patterns :
					del(self.data[pattern])

	def rpi(self, pattern) :
		"""
		calculates the reliability of a pattern (rpi)
		
		a pattern's reliability is computed over all instances of occurrence. For each
		single instance, the PMI of pattern and instance is multiplied with the instance's
		reliability and divided by the highest pmi in the collection
		The sum of these scores is averaged over the number of isntances the pattern extracted
		"""
		rpi = pattern.rpi
		# calculate rt for all instances of the pattern
		for instance in self.data[pattern] :
			instance.rt = self.rt(instance)
		sum = 0.
		for instance in self.data[pattern]:
			sum += (self.pmi(instance, pattern) * instance.rt)/self.max_pmi
		rpi = sum/len(self.data[pattern])
		return rpi
	
	def avg_rpi(self) :
		"""calculates the average from all pattern reliability scores found in the collection"""
		sum = 0.
		for pattern in self.get_patterns():
			if pattern.rpi:
				sum += pattern.rpi
		return sum/len(self.get_patterns())
	
	def rt(self, instance) :
		"""
		calculates the reliability of an instance (rt)
		
		similar to pattern reliabilty, instance reliability also factors in the reliabilty
		of all the patterns that extracted the instance. This is multiplied by the PMI between
		instance and pattern and  divided by maximum pmi, then averaged
		"""
		rt = instance.rt
		if not instance.seed : # no need to calculate rt if it is a seed
			sum = 0.
			for pattern in self.get_patterns_by_instance(instance) :
				sum += (self.pmi(instance, pattern) * pattern.rpi)/self.max_pmi
			rt = sum/len(self.get_patterns_by_instance(instance))
		return rt
	
	
	def pmi(self, instance, pattern) :
		"""
		calculates the Pointwise Mutual Information between a pattern and an instance
		
		PMI(pattern, instance) = log(#cooccurences of pattern and instance/(#occurences pattern*#occurences instance))
		this score is multiplied by a discounting factor to remove bias for infrequent events, normalised and adjusted to 
		a value between 0 and 1
		"""
		c_pi = self.data[pattern].count(instance) # frequency of pattern-instance co occurence
		c_p = len(self.data[pattern]) # frequency of pattern
		c_i = self.count_instance(instance) # frequency of instance across all patterns
		pmi = math.log(c_pi/(c_p*c_i)) # standard Espresso pmi
		pmi *= ( c_pi / (c_pi+1) ) * ( min(c_i,c_p) / (min(c_i,c_p)+1) ) # discounting factor as proposed by Pantel and Ravichandran
		if c_pi != 1 : # prevent zero division
			pmi /= -math.log(c_pi) # normalise pmi
		pmi = (pmi * 0.5) + 0.5 # adjust pmi to range [0,1]
		return pmi
