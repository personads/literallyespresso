class CorpusReader :
	"""
	reads the NYU English Wikipedia Corpus
	
	corpus can be read in the following modes:
	pos: sentence by sentence, including POS-Tags *standard mode, recommended for use with the Espresso implementation
	sen: sentence by sentence
	raw: line by line, including markup, token, lemma, POS, NE and link URL info
	"""
	def __init__(self, file, mode='pos') :
		self.file = file
		self.mode = mode
		self.doc_title = None # current document title
		self.fop = None
		self.file_iter = None
		try :
			self.fop = open(file, 'r', encoding='utf8')
			self.file_iter = iter(self.fop)
		except IOError as ioerr :
			if self.fop :
				self.fop.close()
			print('Error : IOError occured while opening "' + file + '"')
	
	def __iter__(self) :
		return self
	
	def __next__(self) :
		res = ''
		line = next(self.file_iter)
		if line.startswith('#s-doc') : # read document title
			self.doc_title = line.split('\t')[2]
		if self.mode == 'pos' : # construct pos-tagged sentence
			if line.startswith('#s-sent') :
				try :
					line = next(self.file_iter)
					while not line.startswith('#e-sent') :
						line_split = line.split('\t')
						res += line_split[0] + '/' + line_split[5] + ' '
						line = next(self.file_iter)
					res = res[:-1] + '\n'
				except StopIteration as sierr :
					return res
					raise sierr
		elif self.mode == 'sen' : # read sentence as is
			if line.startswith('#s-sent') :
				res = line.split('\t')[3]
		elif self.mode == 'raw' : # read raw line
			res = line				
		return res
	
	def reset_iter(self) :
		self.close_file()
		try :
			self.fop = open(self.file, 'r', encoding='utf8')
			self.file_iter = iter(self.fop)
		except IOError as ioerr :
			if self.fop :
				self.fop.close()
			print('Error : IOError occured while opening "' + self.file + '"')
	
	def close_file(self) :
		if self.fop :
			self.fop.close()
