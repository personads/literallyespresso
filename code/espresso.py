from corpus_reader import CorpusReader
from instance import Instance
from pattern import Pattern
from pattern_collection import PatternCollection
import re
from sys import stdout

class Espresso :
	"""
	main class for running espresso algorithm as described in (Pantel and Pennacchioti, 2008)
	
	extracts semantic relations from a corpus given a set of seed instances
	for the phases of the algorith, see comments in code or README.txt
	results are then written to the specified output file
	
	INPUTS
	out_file: location (including file name) for output file
	corpus_file: location of corpus file
	corpus_mode: pos - reads POS-tagged sentences - recommended, other options require changes to main code
				 sen - reads sentences without tags
				 raw - reads the corpus file line by line
			     also see corpus_reader.py
	seeds: our implementation requires a seed of pattern and instance combined. Format should be as follows:
			{Pattern(r'pattern regex, including POS-Tags and the generic term regex for instances', rpi=1.):[Instance(('Entity1, including POS-Tags', 'Entity2, including POS-Tags'), rt=1., seed=True]}
	gain_threshold: the algorithm will terminate, if less than gain_threshhold patterns are added in an interation
					arbitrarily defaults to 5
	improv_threshold: the algorithm will terminate if the average rpi-score of the collected patterns is less than improv_threshhold*the previous average
					  arbitrarily defaults to 1.5
	k_threshold: the pattern list is shortened to k_threshold patterns after each iteration
				 arbitrarily defaults to 42
	"""
	# init
	def __init__(self, corpus_file, corpus_mode = 'pos', seeds = {}, term_regex = r'(((\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)+(\w+/IN\s)?(\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)*)?\w+/NN(S|PS?)?)', gain_threshold = 5, improv_threshold = 1.5, k_threshold = None):
		"""
		"""
		self.corpus_file = corpus_file
		self.corpus_mode = corpus_mode
		self.seeds = seeds
		self.term_regex = term_regex
		self.gain_threshold = gain_threshold
		self.improv_threshold = improv_threshold
		self.k_threshold = k_threshold
		
		self.patterns = PatternCollection(self.seeds, self.term_regex)
		self.corpus = CorpusReader(self.corpus_file, mode=self.corpus_mode)
		self.prev_len = 0
		self.prev_avg_rpi = 0
		self.iteration = 0
		
	def run(self, iteration_limit=None):
		"""
		runs the Espresso algorithm
		
		specify the number of iterations using the iteration_limit parameter.
		If no limit id given, the algorithm will run until the break conditions given
		at initialisation are reached (gain_threshold, improv_threshold)
		"""
		while True:		
			print('\nEspresso (iteration:' + str(self.iteration) + ') (patterns:' + str(len(self.patterns)) + ') (instances:' + str(len(self.patterns.get_instances())) + ')')
			
			# pattern acquisition
			line_n = 0
			ext_n = 0
			for line in self.corpus :
				if (line_n%10000) == 0:
					stdout.write('\rpattern acquisition : %d patterns | %d lines' % (ext_n, line_n))
					stdout.flush()
				cur_instances = self.patterns.is_instance_match(line)
				for instance in cur_instances :
					for pattern in self.patterns.ext_patterns(line, instance) : # extract patterns by matched instances
						if not self.patterns.has_pattern(pattern) : # prevent overwriting existing patterns
							pattern.rpi = instance.rt # initial rpi cancels out to the instance used to extract the pattern
							self.patterns.add_pattern(pattern)
							ext_n += 1
				line_n += 1
			stdout.write('\rpattern acquisition : done              ' + (' ' * (len(str(line_n))+len(str(ext_n))) + '\n'))
			
			# instance acquisition
			self.corpus.reset_iter()
			line_n = 0
			ext_n = 0
			for line in self.corpus :
				if (line_n%10000) == 0:
					stdout.write('\rinstance acquisition : %d instances | %d lines' % (ext_n, line_n))
					stdout.flush()
				cur_patterns = self.patterns.is_pattern_match(line)
				for pattern in cur_patterns :
					for instance in self.patterns.ext_instances(line, pattern) : # extract instances by matched patterns
						self.patterns.add_instance(instance, pattern)
						ext_n += 1
				line_n += 1
			stdout.write('\rinstance acquisition : done               ' + (' ' * (len(str(line_n))+len(str(ext_n))) + '\n'))
					
			# pattern ranking
			stdout.write('\rpattern ranking : ...')
			stdout.flush()
			self.patterns.rank() # rank all patterns and instances
			if self.k_threshold is None :
				self.patterns.select(self.prev_len + 1) # select top k
			else :
				self.patterns.select(self.k_threshold) # select top k
			self.patterns.lock_patterns() # lock all patterns in the iteration
			self.corpus.reset_iter() # reset corpus reader iterator
			self.iteration += 1
			stdout.write('\rpattern ranking : done\n')
			
			# iteration evaluation
			if iteration_limit == None :
				if ( len(self.patterns) < (self.prev_len + self.gain_threshold)  ) or ( self.patterns.avg_rpi() < (self.prev_avg_rpi * self.improv_threshold) ) :
					break
				self.prev_len = len(self.patterns)
				self.prev_avg_rpi = self.patterns.avg_rpi()
			else :
				if self.iteration >= iteration_limit :
					break
	
	def out(self, out_file) :
		"""writes the list of patterns and instances to the specified output file"""
		try :
			with open(out_file, 'w', encoding='utf8') as fop :
				fop.write(str(self.patterns))
		except IOError :
			print('Error: :(')
	
	def serialize(self) :
		"""returns the Espresso class instance in an XML-like format"""
		res = '<espresso corpus="' + str(self.corpus_file).replace('"', '\'') + '" corpus_mode="' + self.corpus_mode + '" k="' + str(self.k_threshold) + '" gain="' + str(self.gain_threshold) + '" improv="' + str(self.improv_threshold) + '" iterations="' + str(self.iteration) + '" term_regex="' + self.term_regex.replace('"', '\'') + '">\n'
		for line in self.patterns.serialize().split('\n') :
			res += '\t'+ line + '\n'
		res += '<\espresso>'
		return res

	@staticmethod
	def load(file) :
		"""deserializes an Espresso class"""
		res = None
		with open(file, 'r') as fop :
			try :
				ext_attr = re.compile(r'\s+([^\s]+?)="(.+?)"')
				collection_data = {}
				lines = fop.readlines()
				line_n = 0
				while line_n < len(lines) :
					if re.match(r'\s*<espresso\s.*', lines[line_n]) :
						corpus_file = None
						corpus_mode = None
						iterations = None
						k_threshold = None
						gain_threshold = None
						improv_threshold = None
						term_regex = None
						for attr in ext_attr.findall(lines[line_n]) :
							if attr[0] == 'corpus' :
								corpus_file = attr[1].replace('\'', '"')
							if attr[0] == 'corpus_mode' :
								corpus_mode = attr[1]
							if attr[0] == 'iterations' :
								iterations = int(attr[1])
							if attr[0] == 'k' :
								k_threshold = int(attr[1])
							if attr[0] == 'gain' :
								gain_threshold = int(attr[1])
							if attr[0] == 'improv' :
								improv_threshold = float(attr[1])
							if attr[0] == 'term_regex' :
								term_regex = attr[1].replace('\'', '"')
						res = Espresso(corpus_file, corpus_mode, term_regex=term_regex, gain_threshold=gain_threshold, improv_threshold=improv_threshold, k_threshold=k_threshold)
						res.iteration = iterations
						while (not re.match(r'\s*</espresso>.*', lines[line_n])) and (line_n < len(lines)-1) :
							if re.match(r'\s*<collection\s.*', lines[line_n]) :
								term_regex = None
								for attr in ext_attr.findall(lines[line_n]) :
									if attr[0] == 'term_regex' :
										term_regex = attr[1].replace('\'', '"')
								res.patterns = PatternCollection(term_regex=term_regex)
								while (not re.match(r'\s*</collection>.*', lines[line_n])) and (line_n < len(lines)-1) :
									if re.match(r'\s*<pattern\s.*', lines[line_n]) :
										regex = None
										rpi = None
										locked = None
										for attr in ext_attr.findall(lines[line_n]) :
											if attr[0] == 'regex' :
												regex = attr[1]
											if attr[0] == 'rpi' :
												rpi = float(attr[1])
											if attr[0] == 'locked' :
												if attr[1] == 'True' :
													locked = True
												elif attr[1] == 'False' :
													locked = False
										cur_pattern = Pattern(regex, rpi, locked)
										res.patterns.add_pattern(cur_pattern)
										while (not re.match(r'\s*</pattern>.*', lines[line_n])) and (line_n < len(lines)-1) :
											if re.match(r'\s*<instance\s.*', lines[line_n]) :
												rt = None
												seed = None
												for attr in ext_attr.findall(lines[line_n]) :
													if attr[0] == 'rt' :
														rt = float(attr[1])
													if attr[0] == 'seed' :
														if attr[1] == 'True' :
															seed = True
														elif attr[1] == 'False' :
															seed = False
												instance_data = []
												while (not re.match(r'\s*</instance>.*', lines[line_n])) and (line_n < len(lines)-1) :
													for attr in ext_attr.findall(lines[line_n]) :
														if attr[0] == 'data' :
															instance_data.append(attr[1])
													line_n += 1
												cur_instance = Instance(tuple(instance_data), rt=rt, seed=seed)
												res.patterns.data[cur_pattern].append(cur_instance)
											line_n += 1
									line_n += 1
							line_n += 1
					line_n += 1
			except :
				print('Error : Could not load "' + file + '" as Espresso :/')
		return res