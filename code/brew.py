'''
	offers a command-line user interface to start the Espresso algorithm
	
	call "brew.py -h" for a detailed description of the parameters
	default usage:
		brew.py [-options] corpus_file seed_file output_prefix
'''
from pattern import Pattern
from instance import Instance
from pattern_collection import PatternCollection
from espresso import Espresso
from sommelier import Sommelier
import re
import argparse

# argument parsing
arg_parser = argparse.ArgumentParser(description='brew a strong and filtered cup of Espresso')
arg_parser.add_argument('corpus', help='path to the corpus to be used')
arg_parser.add_argument('seeds', help='path to initial seeds for Espresso')
arg_parser.add_argument('out', help='output prefix')
arg_parser.add_argument('-cm', '--corpus_mode', help='mode of CorpusReader')
arg_parser.add_argument('-tr', '--term_regex', help='regex used as term placeholder')
arg_parser.add_argument('-gt', '--gain_threshold', type=int, help='gain-threshold for pattern extraction')
arg_parser.add_argument('-it', '--improv_threshold', type=float, help='improvement-threshold for iteration evaluation')
arg_parser.add_argument('-iter', '--iterations', type=int, help='number of iterations to be forgone')
arg_parser.add_argument('-k', '--k_threshold', type=int, help='k-threshold for pattern selection')
arg_parser.add_argument('-s', '--sommelier', action='store_true', help='start sommelier evaluation on completion')
args = arg_parser.parse_args()

print('\nLiterally Espresso\n')

if not args.corpus_mode :
	args.corpus_mode = 'pos'
if not args.term_regex :
	args.term_regex = r'((\w+/DT\s)?((\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)+(\w+/IN\s)?(\w+/DT\s|\w+/JJ[RS]?\s|\w+/NN(S|PS?)?\s)*)?\w+/NN(S|PS?)?)'
if not args.gain_threshold :
	args.gain_threshold = 5
if not args.improv_threshold :
	args.improv_threshold = 1.5
if not args.k_threshold :
	args.k_threshold = None

seed_collection = PatternCollection.load(args.seeds)

if seed_collection is None :
	seed_collection= {}
robusta = Espresso(
args.corpus, 
corpus_mode = args.corpus_mode, 
seeds = seed_collection.data, 
term_regex = args.term_regex, 
gain_threshold = args.gain_threshold, 
improv_threshold = args.improv_threshold, 
k_threshold = args.k_threshold)

if args.iterations :
	robusta.run(args.iterations)
else :
	robusta.run()
robusta.out(args.out + '_out')
with open(args.out + '_serialized', 'w') as fop :
	fop.write(robusta.serialize())

if args.sommelier :
	flavour = Sommelier(robusta)
	print()
	flavour.taste()
	print('\nThis Espresso\'s precision is', flavour.precision)

print('\ndone.')
