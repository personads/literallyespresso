from corpus_reader import CorpusReader

cr = CorpusReader('../data/wiki_shakespeare.txt', mode='pos')
with open('tst_corpus_reader_out.txt', 'w', encoding='utf8') as fop :
	for line in cr :
		fop.write(line)
	fop.write('===========================================')
	cr.reset_iter()
	for line in cr :
		fop.write(line)
cr.close_file()